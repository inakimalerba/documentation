#!/bin/bash

set -euo pipefail

hugo server &
trap 'trap - EXIT && kill -- -$$' EXIT
sleep 3

# ignore
# - legacy favicons
# - GitLab URLs requiring login
# - GitLab links to markdown anchors that cannot be resolved as markdown is rendered dynamically
# - Red Hat internal websites
muffet http://localhost:1313/ \
    -e '.*favicon.ico$' \
    -e 'https://gitlab.com/cki-project/documentation/-/edit/.*' \
    -e 'https://gitlab.com/cki-project/documentation/-/issues/new?.*' \
    -e 'https://gitlab.com/cki-project/documentation/-/forks/new' \
    -e 'https://gitlab.com/cki-project/documentation/-/commit/.*' \
    -e 'https://gitlab.com/cki-project/.*#.*' \
    -e 'https://projects.engineering.redhat.com/.*'

# if no dangling links were found, shut down normally
kill %1
trap - EXIT
